package INF101.lab2;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.NoSuchElementException;

public class Fridge implements IFridge {
    private int capacity = 20;

    private int n = 0;

    ArrayList<FridgeItem> itemsinfridge = new ArrayList<FridgeItem>();

    /** private FridgeItem fridgeitems[] = new FridgeItem[capacity]; */

    /**
     * Returns the number of items currently in the fridge
     *
     * @return number of items in the fridge
     */
    public int nItemsInFridge(){
        return n;
    }

    /**
     * The fridge has a fixed (final) max size.
     * Returns the total number of items there is space for in the fridge
     *
     * @return total size of the fridge
     */
    public int totalSize(){
        return capacity;
    }

    /**
     * Place a food item in the fridge. Items can only be placed in the fridge if
     * there is space
     *
     * @param item to be placed
     * @return true if the item was placed in the fridge, false if not
     */
    public boolean placeIn(FridgeItem item){
        if (nItemsInFridge() < totalSize())  {
             itemsinfridge.add(item);
             n += 1;
             return true; }
        else {
            return false;}
    }

    /**
     * Remove item from fridge
     *
     * @param item to be removed
     * @throws NoSuchElementException if fridge does not contain <code>item</code>
     */
    public void takeOut(FridgeItem item){
        boolean foundit = false;
        for (FridgeItem itemr: itemsinfridge){
            if (itemr.equals(item)) {
                foundit = true;
            }
        }
        if (foundit != true) {
            throw new NoSuchElementException(); }
        else {
            itemsinfridge.remove(item);
            n -= 1;
        }
    }
    /**
     * Remove all items from the fridge
     */
    public void emptyFridge(){
        this.n = 0;
        this.itemsinfridge.clear();
        /**fridgeitems.length = 0;    */
    }

    /**
     * Remove all items that have expired from the fridge
     * @return a list of all expired items
     */
    public List<FridgeItem> removeExpiredFood(){
        ArrayList<FridgeItem> expiredFood = new ArrayList<FridgeItem>();
        FridgeItem item;
        for (int i = 0; i < itemsinfridge.size(); i++){
            item = itemsinfridge.get(i);
            if (item.hasExpired()) {
                item = itemsinfridge.remove(i);
                expiredFood.add(item);
                n -= 1;
                i -= 1;
            }
        }
        return expiredFood;
    }
}
